import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import authSerVice from '@/services/auth'
import { useMessageStore } from './message'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'


export const useAuthStore = defineStore('auth', () => {
  const messageStore = useMessageStore()
  const router = useRouter()
  const loadingStore = useLoadingStore()
  const login = async function (email: string, password: string) {
    loadingStore.doLoad()
    try {
      const res = await authSerVice.login(email, password)
      console.log(res.data)
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', JSON.stringify(res.data.access_token))
      messageStore.showMessage('Login Success')
      router.replace('/')
    } catch (e: any) {
      console.log(e)
      messageStore.showMessage(e.message)
    }
    loadingStore.finish()

  }
  const logout = function () {
    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
    router.replace('/login')
  }
  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser === null) return null
    return JSON.parse(strUser)
  }

  function getToken(): String | null {
    const strtoken = localStorage.getItem('access_token')
    if (strtoken === null) return null
    return JSON.parse(strtoken)
  }
  return { getCurrentUser, login, getToken, logout }
})
